# RSYNC

### Funcionamento

`rsync [opcoes] fonte destino`

`-r` para ser recursivo e compiar subdiretórios

`-v` para ser verboso com mais detalhes

`-e` para executar um comando, ou informar uma porta ssh diferente da padrao (rsync -vre 'ssh -p 55022' fonte usuario@192.168.0.5:/tmp)

`-a` habilita modo de arquivamento (não é necessário o -r), já transfere com as propriedades dos arquivos

`-h` saída legível para humanos

`-z` comprimidos os arquivos enquanto transfere

`--delete` faz uma sincronia total, se no fonte não existir o arquivo, sera removido do destino

`--exclude=*.png` para não copiar todos os arquivos .png por exemplo ou

`--exclude=Diretorio/*.png` para não copiar todos os arquivos .png do diretório específico por exemplo

`fonte/` se utilizar a / no final ele copiará o conteúdo do diretório

`fonte` se utilizar sem a / então será copiado o fonte como um diretório

`destino` pode ser um diretório na maquina local (informar diretório) ou via ssh (usuario@ip/dominio:caminho diretorio)