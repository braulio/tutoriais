set t_Co=256
let g:solarized_termcolors=256
syntax enable
set autoindent
set background=dark
syntax on
colorscheme atom-dark-256
"set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<,space:.
set listchars=tab:>-,trail:~,extends:>,precedes:<
set list
:set title titlestring=
"Spell-check set to F6:
map <F12> :w!<CR>:!aspell -c % --encoding=utf-8<CR>:e! %<CR>
map <F6> :setlocal spell! spelllang=pt_br<CR>
map <F7> :set spelllang=en_us<CR>

set wildmode=longest,list,full
set wildmenu

"Arquivo de configuração cedido pelo gpxlnx
" use spaces instead of tabs
" https://vim.fandom.com/wiki/Converting_tabs_to_spaces
set tabstop=4 shiftwidth=4 expandtab

" Make backspace remove 4 spaces
set softtabstop=4

" insert real tabs with shitt+tab
inoremap <S-Tab> <C-V><Tab>

" incremental search
set incsearch

" highlight all search matches
set hlsearch

" show cmd bar
set showcmd

" show line editing highlight
"set cursorline

"toggle line counter with ctrl+n, enabled by default
"set number
nmap <C-N> :set invnumber<CR>

"My leaders
let mapleader = " "

"open new shell
nnoremap <leader>. :shell<CR>

"quit file
nnoremap <leader>q :q<CR>
nnoremap <leader>Q :q!<CR>

"save file
nnoremap <leader>w :w<CR>
nnoremap <leader>W :w!<CR>

"split screen
nnoremap <leader>ss :split<CR>
nnoremap <leader>vv :vsplit<CR>

"switch between windows
map <leader>h :wincmd h<CR>
map <leader>j :wincmd j<CR>
map <leader>k :wincmd k<CR>
map <leader>l :wincmd l<CR>

"faster switch
nnoremap ,, <C-w>w

"change windows size
nnoremap <silent> <leader><Right> :vertical resize +5<CR>
nnoremap <silent> <leader><Left> :vertical resize -5<CR>
nnoremap <silent> <leader><Up> :resize -5<CR>
nnoremap <silent> <leader><Down> :resize +5<CR>

"Git Fugitive
nnoremap <leader>G :G<space>
