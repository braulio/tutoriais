# TUTORIAL DE INSTALAÇÃO E CONFIGURAÇÃO BÁSICA

### Primeiro verificar configuração de horário e data

`dpkg-reconfigure tzdata`

`systemctl restart rsyslog`

### Instalar Pacote FAIL2BAN

`apt install fail2ban`

## Configurar

`cd /etc/fail2ban/`

`fail2ban.conf` arquivo de configuração geral do fail2ban

`jail.conf` arquivo de "profiles" de vários serviços

`jail.d` diretório para colocar arquivos de profiles por exemplo: ssh.local, apache.local

`cp jail.conf jail.local` recomendado criar um novo arquivo com .local para evitar que o sistema sobrescreva o arquivo em uma atualização

### jail.local

```
[DEFAULT]

bantime = 60m

findtime = 10m

maxretry = 5
```

```
[SSHD]

enabled  = true
mode   = normal
port    = 55022
logpath = %(sshd_log)s
backend = %(sshd_backend)s
```

```
[dropbear]

[APACHE...]

[mysql-auth]
```

### Para criar arquivo configuração para Nexcloud

`nano /etc/fail2ban/jail.d/nextcloud.local` para criar um arquivo novo para configuração do nextcloud no fail2ban

```
[nextcloud]
backend = auto
enabled = true
port = 80,443
protocol = tcp
filter = nextcloud
maxretry = 3
bantime = 86400
findtime = 43200
logpath = /var/ncdata/nextcloud.log
```
## Pós Configuração

### Verificar status e iniciar servidor

`systemctl status fail2ban` para verificar o status e confirmar que está desativado

`systemctl start fail2ban` para iniciar o serviço do fail2ban

`systemctl restart fail2ban` para reinicar servidor fail2ban após alteração nas configurações

### Monitoramento

`fail2ban-client ping` para receber o "pong" do servidor e confirmar que está ativo

`fail2ban-client status` para verificar todas as jaulas ativadas

`fail2ban-client status nextcloud` para verificar status da jaula nextcloud por exemplo

`fail2ban-client banned` pra vizualizar todas as jaulas com as quantidades de IPs banidos

`fail2ban-client start` `fail2ban-client stop` Para iniciar ou parar fail2ban

`fail2ban-client restart nomedajaula` para reiniciar a jaula especifica

`fail2ban-client -h` para mais comandos