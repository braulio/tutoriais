#!/usr/bin/env bash
# ---------------------------------------------------------------
# Script   : demo-test.sh
# Descrição: Script para demonstrar sobre programa Test
# Versão   : 0.1
# Autor    : Braulio Henrique Marques Souto <braulio@disroot.org>
# Data     : 21/06/2022
# Licença  : GNU/GPL v3.0
# ---------------------------------------------------------------
# Uso: ./demo-test.sh ou /caminho/demo-test.sh
# ---------------------------------------------------------------


# "Limpar" o terminal para melhor vizualização
clear

# Mensagem
echo "O Objetivo desse script é comparar o seu número com o número '5'."
read -p "Entre com um número de 1 a 10: " numero

# Tratar Erros

# =~ indica que a expressão da direita é uma REGEX
# ! negação ou inverso
[[ ! $numero =~ ^[0-9]+$ ]] && echo "Erro! Digite um *NÚMERO*!" && exit 1

# -le = less or equal
[[ $numero -gt 10 || $numero -le 0 ]] && echo "Erro! Seu número deve ser entre 1 a 10." && exit 1

# Respostas normais

# -eq = equal
[[ $numero -eq 5 ]] && echo "Acerto, seu número é igual a 5!" && exit 0

# -lt = less than
[[ $numero -lt 5 ]] && echo "Quase lá, seu número é MENOR que 5" && exit 0

# -gt = greater than
[[ $numero -gt 5 ]] && echo "Quase lá, seu número é MAIOR que 5" && exit 0
