# Certbot DNS Frenoom.com

`vi /var/local/credentials.ini` criar arquivo credentials.ini com o conteúdo:

> Baseado Salsa/Debian
```
certbot_dns_freenom:dns_freenom_username = "username"
certbot_dns_freenom:dns_freenom_password = "password"
```

ou

> Baseado GitHub
```
dns_freenom_username = "username"
dns_freenom_password = "password"
```

`chmod 600 /var/local/credentials.ini` alterar permissão do arquivo credentials.ini

### linha de comando para usar o certbot para gerar certificado pelo DNS-Chalenge TXT no freenom.com

> Baseado Salsa/Debian
```
certbot certonly -a certbot-dns-freenom:dns-freenom  \
  --certbot-dns-freenom:dns-freenom-credentials /var/local/credentials.ini \
  --certbot-dns-freenom:dns-freenom-propagation-seconds 1800 \
  -d "*.braulio.ml" \
  -m braulio@disroot.org \
  --agree-tos -n
```

> Baseado GitHub
```
certbot certonly -a dns-freenom \
  --dns-freenom-credentials /var/local/credentials.ini \
  --dns-freenom-propagation-seconds 1800 \
  -d "*.example.com" \
  -m admin@example.com \
  --agree-tos -n
```

### caminho do certificado gerado pelo certbot

`/etc/letsencrypt/live/braulio.ml/fullchain.pem`

### caminho da chave gerada pelo certbot

`/etc/letsencrypt/live/braulio.ml/privkey.pem`

## Origem e dependências:

- Origem Debian: `https://salsa.debian.org/letsencrypt-team/certbot/certbot-dns-freenom`

- Origem Github: `https://github.com/Shm013/certbot-dns-freenom`

### Dependência:

- Pacote Debian: `https://tracker.debian.org/pkg/python-freenom`

- Origem Upstream: `https://github.com/Shm013/freenom-dns`
