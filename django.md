# DJANGO

## Dependências

Debian
`apt install python3 python3-pip python3-venv`

Windows
`https://www.python.org/downloads/windows/`

## Ambiente Virtual

### Criar

De dentro do diretório raiz do projeto
Debian (GNU-Linux)
`python3 -m venv venv`

Windows
`python -m venv venv`

### Ativar

Apenas depois de ter criado:
Debian (GNU-Linux)
`source <venv>/bin/activate`

Windows CMD
`<venv>\Scripts\activate.bat`

Windows PowerShell
`<venv>\Scripts\Activate.ps1`

#### Atualizando PIP ou pacotes pip

`pip install -U <pip>` ou `pip install --upgrade <pip>`

### Desativar

`deactivate`

## Instalação Django

com o ambiente virtual ativado e na pasta raiz do projeto
`pip install Django`

### Verificando versão instalada

`python -m django --version`

## Uso

### Novo Projeto

`django-admin startproject <meusite> .`

### Novo Aplicativo

`python manage.py startapp <aplicativo1>`

E após criar, é preciso registrar ele dentro das configurações do projeto em INSTALLED_APPS
`<aplicativo1>,`

### Iniciando Servidor Desenvolvimento

`python manage.py runserver`

> Por padrão irá executar em http://127.0.0.1:8000
> Para expor para rede interna e alterar a porta, segue exemplos

Alterando a porta padrão
`python manage.py runserver 8080`

Expondo o servidor na rede local atraves do 0.0.0.0
`python manage.py runserver 0.0.0.0:8000`

### Analisando Alteração Banco de Dados ou Iniciando Banco de Dados

`python manage.py makemigrations`

### Executando/Aplicando as tarefas no Banco de Dados

`python manage.py migrate`

### Criando SuperUsuario

É necessário já ter feito as migrações antes de criar super usuario
`python manage.py createsuperuser`

## Settings

```python
# BASICO - Alterar em todos
import os # se optar por utilizar os.path.join

"DIRS": [os.path.join(BASE_DIR, "templates")],
"DIRS": [BASE_DIR / "src" / "templates",],

LANGUAGE_CODE = "pt-br"

TIME_ZONE = "America/Sao_Paulo"

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "templates/static"),
    BASE_DIR / "src" / "assets",
]

STATIC_ROOT = os.path.join("static")
STATIC_ROOT = BASE_DIR / "static"

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_ROOT = BASE_DIR / "media"
MEDIA_URL = "media/"
########## FINAL BASICO ##########

# Config Messages
from django.contrib.messages import constants
MESSAGE_TAGS = {
    constants.DEBUG: "alert-primary",
    constants.ERROR: "alert-danger",
    constants.SUCCESS: "alert-success",
    constants.INFO: "alert-info",
    constants.WARNING: "alert-warning",
}

# Indica qual é o caminho para a página de login
LOGIN_URL = 'login'
# Redireciona para home URL após login (Padrão redireciona para /accounts/profile/)
LOGIN_REDIRECT_URL = 'home'
# Redireciona para URL login após logout
LOGOUT_REDIRECT_URL = 'login'

# Config e-mail
# Para envio do email apenas pelo terminal para Desenvolvimento
if DEBUG:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

# Para envio do email real, em produção
if not DEBUG:
    EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
    EMAIL_HOST = config("EMAIL_HOST")
    EMAIL_HOST_USER = config("EMAIL_HOST_USER")
    EMAIL_HOST_PASSWORD = config("EMAIL_HOST_PASSWORD")
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True
    DEFAULT_FROM_EMAIL = "noreply@meudominio.com.br"
    SITE_ID = 1
    SITE_NAME = "Projeto..."
    SITE_DOMAIN = "meudominio.com.br"

# Definir o tamanho maximo para salvar na memoria RAM (padrão 2621440 -> 2.5 MB)
FILE_UPLOAD_MAX_MEMORY_SIZE = 104857600 # 100 MB em bytes
# Definir quantidade maxima de arquivos por requisicao (padrao 100 arquivos)
DATA_UPLOAD_MAX_NUMBER_FILES = 150
# Definir quantidade maxima de parametros por requisicao (padrao 1000 GET ou POST)
DATA_UPLOAD_MAX_NUMBER_FIELDS = 1500
# None para desativar em qualquer uma acima
### SEGURANÇA ### PARA DEPLOY
from decouple import config

SECRET_KEY = config("SECRET_KEY")

DEBUG = config("DEBUG", cast=bool, default=False)

ALLOWED_HOSTS = [h.strip() for h in config("ALLOWED_HOSTS", "").split(",") if h.strip()]
CSRF_TRUSTED_ORIGINS = ['https://subdomain.example.com', 'https://*.example.com']
SECURE_HSTS_SECONDS = 31536000 # security.W004, One year in seconds
SESSION_COOKIE_SECURE = True # security.W012
CSRF_COOKIE_SECURE = True # security.W016
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
SECURE_CONTENT_TYPE_NOSNIFF = True

CORS_ALLOW_CREDENTIALS = True
CORS_ALLOWED_ORIGINS = ["http://127.0.0.1:*", 'https://api.example.com']
# CORS_ORIGIN_ALLOW_ALL = True

DATABASES = {
    "default": {
        "ENGINE": config("DB_ENGINE", "change-me"),
        "NAME": config("POSTGRES_DB", "change-me"),
        "USER": config("POSTGRES_USER", "change-me"),
        "PASSWORD": config("POSTGRES_PASSWORD", "change-me"),
        "HOST": config("POSTGRES_HOST", "change-me"),
        "PORT": config("POSTGRES_PORT", "change-me"),
    }
}
```

## Models

Link tipos Campos `https://docs.djangoproject.com/en/5.0/ref/models/fields/`

Link programa para visualizar banco de dados sqlitebrowser
`https://sqlitebrowser.org/`

Muitos para Um `ForeignKey`

Muitos para Muitos `ManyToManyField`

Um para Um `OneToOneField`

### CRUD

#### Read

```python
def listagem(request):
    data = {}
    data["frutas"] = Fruta.objects.all()  # Poderia ser .filter() .first() .last()
    return render(request, "aplicativo1/lista_frutas.html", data)
```

#### Create

```python
def newFruta(request):
    form = FrutaForm(request.POST or None) # Se houver dados no POST form sera preenchido senão formulário em branco

    if form.is_valid():
        form.save()
        return redirect("listagem")

    return render(request, "aplicativo1/nova_fruta.html", {"form": form})
```

#### Update

```python
def newFruta(request, pk):
    fruta = Fruta.objects.get(pk=pk) # .get() retorna apenas 1 objeto e .filter() vários
    form = FrutaForm(request.POST or None, instance=fruta) # Se houver dados no POST form sera preenchido senão formulário em branco

    if form.is_valid():
        form.save()
        return redirect("listagem")

    return render(request, "aplicativo1/nova_fruta.html", {"form": form, "aluno": aluno})
```

#### Delete

```python
def newFruta(request, pk):
    fruta = Fruta.objects.get(pk=pk) # .get() retorna apenas 1 objeto e .filter() vários
    fruta.delete()
    return redirect("listagem")
```

### Adicionando Models na Área administrativa do Django

Dento do arquivo `admin.py` importar a Classe da Model desejada e adicionar seguinte linha:
`admin.site.register(MinhaClasse)`

## DEPLOY

Alterar para um secret key verdadeiro, segue comando
`python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'`

Verificar a segurança: `python manage.py check --deploy`

## Export and Import Data

### Exportar

- Exportar dados de um APP:
  `python manage.py dumpdata auth --indent 2 > auth.json` considerando que auth é o nome do app a ser exportado

  `python manage.py dumpdata auth --indent 2 -o auth.json` considerando que auth é o nome do app a ser exportado

> --indent 2 é opcional

- Exportar dados de uma tabela:
  `python manage.py dumpdata auth.user --indent 2 > user.json` exportando dados da tabela user do app auth

- Exportar de varios apps ao mesmo temo direcionando para um diretorio:
  `python manage.py dumpdata auth app1 app2 --indent 2 > fixtures/fake_data.json`

### Importar

Primeiro se o projeto for novo, realizar o `migrate`

Depois popular o banco de dados com loaddata

`python manage.py loaddata fake_data.json` unico arquivo

`python manage.py loaddata fake_data.json users.json polls.json` multiplos arquivos de uma vez
