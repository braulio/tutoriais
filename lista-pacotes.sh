#!/usr/bin/env bash

DATA=$(date +%Y%m%d-%H%M)

dpkg --get-selections | sed 's/\s.*//' > lista-pacotes-$HOSTNAME-$DATA.txt

# apt install $(cat ARQUIVO.txt)
# apt install $(< ARQUIVO.txt)
# apt install $(grep '^[^#]' ARQUIVO.txt)
# apt install $(grep --no-filename '^[^#]' ARQUIVO1.txt ARQUIVO2.txt ...)
