# Roteiro para Instalar Nextcloud com Apache2 e MariaDB no Debian 11

Copyright (C) 2021-2022 Braulio <braulio@disroot.org>

Licença Creative Commons Attribution-ShareAlike 4.0
    
Bom aproveito e compartilhe livremente!!!
Em casos de dúvidas XMPP:braulio@jabb3r.org

## Instalar e Configurar no primeiro acesso

### Instalar Pré-Requisitos Apache,MariaDB e PHP para Nextcloud

```
apt install vi vim wget git apache2 mariadb-server php-common libapache2-mod-php php-gd php-mysql php-curl php-mbstring php-intl php-gmp php-bcmath imagemagick php-imagick ffmpeg php-xml php-zip php-bz2 php-redis redis-server python-certbot-apache
```

### Configurar Redis (servidor de cache de memória):

```
vi /etc/redis/redis.conf
```

* descomentar: /var/run/redis/redis-server.sock
* descomentar: unixsocketperm 700 e alterar o valor de 700 para 770
* mudar: port 6379 para port 0
* adicionar usuário do Apache para o grupo Redis usermod -aG redis www-data

### Configurar o Virtual Host no Apache

```
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/nextcloud.conf

vi /etc/apache2/sites-available/nextcloud.conf
```

* descomentar e configurar: ServerName/ServerAdmin/DocumentRoot

```
<VirtualHost *:80>
     ServerName dominio.com
     ServerAdmin admin@dominio.com
     DocumentRoot /var/www/nextcloud
     ErrorLog ${APACHE_LOG_DIR}/error.log
     CustomLog ${APACHE_LOG_DIR}/access.log combined

   <Directory /var/www/nextcloud/>
     Require all granted
     AllowOverride All
     Options FollowSymLinks MultiViews

     <IfModule mod_dav.c>
      Dav off
     </IfModule>
   </Directory>

</VirtualHost>
```

### Ativar o Virtual Host e os modulos requeridos, e reiniciar o Apache

```
a2ensite nextcloud

a2enmod rewrite headers env dir mime

systemctl restart redis apache2
```

### Dowload e extrair Nexcloud, criar diretório Dados, ajustar as permissões e apagar arquivo compactado

```
wget https://download.nextcloud.com/server/releases/latest.tar.bz2.md5

wget https://download.nextcloud.com/server/releases/latest.tar.bz2

tar -xvf ./latest.tar.bz2 -C /var/www/

chown -R www-data: /var/www/nextcloud

mkdir /var/ncdata

chown www-data: /var/ncdata

rm ./latest.tar.bz2
```

### Configurar MariaDB

```
mysql_secure_installation

mariadb

CREATE DATABASE nextcloud;

CREATE USER 'nextcloud'@'localhost' IDENTIFIED BY 'P@ssw0rd';

GRANT ALL PRIVILEGES ON nextcloud . * TO 'nextcloud'@'localhost';

quit
```

### Configurar PHP

```
nano /etc/php/7.4/apache2/php.ini
```

* Alterar

```
memory_limit=512M
output_buffering=Off
```

### Adicionar a configuração do Nextcloud:

```
vi /var/www/nextcloud/config/config.php
```

```
'trusted_domains' =>
array (
  0 => 'dominio.com',
  1 => 'www.dominio.com',
  2 => '192.168.1.5',
),

'htaccess.RewriteBase' => '/',
'default_phone_region' => 'BR',
'default_language' => 'pt-BR',
'default_locale' => 'pt_BR',
'enable_previews' => false,
'memcache.local' => '\\OC\\Memcache\\Redis',
'memcache.distributed' => '\\OC\\Memcache\\Redis',
'redis' =>
array (
  'host' => '/var/run/redis/redis-server.sock',
  'port' => 0,
 ),
'enable_previews' => true,
 'enabledPreviewProviders' =>
 array (
   'OC\Preview\Movie',
   'OC\Preview\PNG',
   'OC\Preview\JPEG',
   'OC\Preview\GIF',
   'OC\Preview\BMP',
   'OC\Preview\XBitmap',
   'OC\Preview\MP3',
   'OC\Preview\MP4',
   'OC\Preview\TXT',
   'OC\Preview\MarkDown',
   'OC\Preview\PDF',
 ),
```

* Para finalizar, recriar .htaccess

```
sudo -u www-data php /var/www/nextcloud/occ maintenance:update:htaccess
```

### Ativar o Agendador de Tarefas CRON

* Mudar para o CRON usando usuário admin em Configuração-Administração-Configurações Básicas
* e depois ativar o crontab

```
  crontab -u www-data -e
```

* e adicionar a seguinte linha

```
*/5  *  *  *  * php -f /var/www/nextcloud/cron.php
```

## Para utilizar o SSL por HTTPS

a2enmod ssl

### CERTIFICADO AUTO-ASSINADO

```
apt install openssl ca-certificates

mkdir /etc/apache2/certs

cd /etc/apache2/certs

openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out dominio.com-cert.crt -keyout dominio.com.key
```

* Editar arquivo /etc/apache2/sites-avaliable/nextcloud.conf e deixar conforme abaixo

```
<VirtualHost *:80>
    ServerName dominio.com
    Redirect permanent / https://dominio.com:55443/
</VirtualHost>

<VirtualHost *:443>
    SSLEngine on
    SSLCertificateFile /etc/apache2/certs/dominio.com-cert.crt
    SSLCertificateKeyFile /etc/apache2/certs/dominio.com.key
     <Directory /var/www/nextcloud/>
      AllowOverride All
      Require all granted
      Options FollowSymLinks MultiViews
       <IfModule mod_dav.c>
        Dav off
       </IfModule>
     </Directory>
    DocumentRoot /var/www/nextcloud
    ServerName dominio.com
    ServerAdmin admin@dominio.com
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    Header add Strict-Transport-Security: "max-age=15552000;includeSubdomains"
</VirtualHost>
```

### Usar o LestEncrypt com Duckdns pelo Challenge DNS-01

### Baixar o dehydrated e depois o hooks

```
git clone https://github.com/dehydrated-io/dehydrated.git

cd dehydrated

mkdir hooks

git clone https://github.com/walcony/letsencrypt-DuckDNS-hook.git hooks/duckdns
```

### Colocar no enviorment do sistema o Token do DuckDns (obtido no site do duckdns.org)

```
export DUCKDNS_TOKEN='token-duckdns'
```

* para vizualizar se o Token esta correto

```
echo $DUCKDNS_TOKEN
```

### Para gerar o certificado

```
./dehydrated -c -d dominio.com -t dns-01 -k 'hooks/duckdns/hook.sh'
```