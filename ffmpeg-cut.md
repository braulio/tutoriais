# PARA CORTAR VÍDEOS COM FFMPEG SEM RECODIFICAR

```
ffprobe video-teste.mp4 2>&1 | grep -i duration
  Duration: 00:01:30.14, start: 0.000000, bitrate: 881 kb/s
```

Quero cortar o vídeo em 3 partes de 30 segundos cada para postar no IG, status do zap ou Facebook:

`ffmpeg -i video-teste.mp4 -ss 00:00:00 -to 00:00:30 -c copy parte1.mp4`

`ffmpeg -i video-teste.mp4 -ss 00:00:30 -to 00:01:00 -c copy parte2.mp4`

`ffmpeg -i video-teste.mp4 -ss 00:01:00 -to 00:01:30 -c copy parte3.mp4`
