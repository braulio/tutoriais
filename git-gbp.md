# GIT E GBP PARA EMPACOTAMENTO DEBIAN

## CONFIGURAR AMBIENTE

### CONFIGURAR GIT
````
git config --global user.name "nome completo"
git config --global user.email email@dominio.org
git config --global core.editor vi
git config --global color.ui true
````
- Para visualizar a configuração:

`git config -l` ou `cat ~/.git/config`


### CONFIGURAR GBP
`/etc/git-buildpackage/gbp.conf` #arquivo de configuração global (para todos usuários) ou para um pacote usar linha abaixo

`.git/gbp.conf` #arquivo configuração apenas para o pacote, .git/ está no diretório do upstream

- Configuração Básica:

    [DEFAULT]

    debian-branch = debian/master

    pristine-tar = True


## COMANDOS BÁSICOS

`git rm` e `git mv` #para remover ou mover um arquivo no diretório sobre controle de versão, e não precisar add ou apagar apenas "commitar"

`git checkout` #para alternar entre as branchs

`git checkout -b nomedabranch` #cria uma branch com o nome passado e o conteúdo da que estava (corrente)

`git checkout nomearquivo` #traz o arquivo de volta se ele foi apagado mas não commitado

`git checkout .` #traz todos arquivos de volta se foi apagado mas não commitado

`git merge devel` #traz o conteúdo(alterações) da branch "devel" para a branch corrente

`git log` #para visualizar todos commits, e pode-se utilizar 7 primeiros caracteres (3bytes e meio)

`git log --name-only` #mostra o nome dos arquivos que foram modificados

`git log tagA..tagB` #para ver os logs apenas entre as tags A/B (necessário o '..' entre as tags)

`git blame nomearquivo` #mostra todas alterações do arquivo com seus respectivos commits

`git diff nomedoarquivo` #mostra as diferenças no arquivo, deve ser usado antes de fazer o commit do arquivo

`git diff commitA commitC` #pegar o commit anterior(A) e o commit posterior(C) para balizamento e sera mostrado entre eles

`git diff tagA tagB` #para ver as diferenças entre as tag

`git show commitA` #mostra o que foi feito no commit(A)

`git grep (regexp)` #procura pelo termo procurado

`gir grep (regexp) $(git rev-list --all)` #procura pelo termo em todos os commits realizados

`git rev-list --all` #lista todos commits realizados

`git revert commitA` #reverter para o commit e ficar descrito em log para todos verem

`git switch -c nilson/1 debian/master`

`git branch` #mostra as branch instaladas no local

`git branch -a` #mostra todas as branch do servidor

`git branch -m debian/master` #para renomear a branch atual para debian/master

`git branch -d debian/sid` #para remover a branch debian/sid

`git branch -D debian/sid` #para remover forçadamente a branch debian/sid

`git commit -a` #commitar tudo e a msg não deve passar de 80 colunas

`git reset --hard HEAD~1` #para voltar para commit HEAD~1 ou fazer com o número do commit como exemplo abaixo

`git reset --hard commitA` #volta para o commit que quer voltar para este ponto

`git tag -a debian/0.1.0-2` #para criar uma tag com o nome debian/0.1.0-2, colocar msg 'Created tag debian/0.1.0-2'

`git remote` #para mostrar o nome do servidor remoto, normalmente origin

`git remote show origin` #mostrar informações de configuração do servidor remoto

`git branch --track style origin/style` #para associar a branch style local a branch remota origin/style

### UNIR COMMITS

`git rebase -i HEAD~1`

`git commit --amend`   #corrige msg do último commit

`git rebase -i HEAD~n` #trocar n pelo número da quantidade dos últimos commits
Substitua `pick` por `reword` antes de cada mensagem do commit que deseja alterar ou `e`.

`git rebase -i commitA` #para entrar no modo interativo e alterar msg e posição dos commits

`git rebase -i --root` #para realizar o rebase desde o primeiro commit

`git push --all; git push --tags` #para enviar todas as branchs e todas as tags para o servidor

`git push --force-with-lease origin example-branch` #para forçar envio do commit que já tinha sido feito push

### GBP

`gbp import-dsc pacote_0.1-1.dsc` #para criar um diretório git com as 3 branchs e empacotamento

`gbp buildpackage` #para criar o orig.tar.gz e constrói o pacote, preciso estar diretório do upstream

`gbp export-orig` #para criar o orig.tar.gz apenas para depois usar debuild, é preciso estar diretório upstream

`gbp tag` #para criar a tag de forma automática se baseando na última entrada do d/changelog

`gbp tag --ignore-new` #para criar a tag e ignorar aviso de arquivos novos

`gbp dch` #para criar automaticamente nova entrada no changelog e todas as alterações na entrada, commit 'Generated changelog'

`gbp import-orig ../v0.2.tar.gz` #para iniciar a nova versão do upstream

`gbp push` #para enviar para os repositórios

`gbp clone vcsgit:nomepacote` #para localizar e baixar diretamente o repositório

`gbp clone salsa:braulio/nomedopacote` #para clonar repositório 'nomedopacote' do userspace Braulio no salsa

`gbp clone github:usuario/nomeprograma` #para clonar repositório 'nomedoprograma' do userspace usuário no github

`dch -i` #para nova entrada no changelog e commitar 'New changelog'

`dch -r` #para marcar como pronto


### COMANDOS "ESPECIAIS"

`debcheckout nomepacote` #para clonar pacote via https através da informação do VCs git d/control

`debcheckout -a nomepacote` #para clonar via git

`origtargz` #obtém o tarball do upstream, deverá ser usado dentro do diretório do upstream

`origtargz -dt` #fará o download sem descompactar o mesmo

`debsnap` #baixa todas as versões ou um intervalo do snapshot.debian.org

`debsanp --list nomedopacote` #mostra as revisões para aquele pacote

`debsnap -v nomedopacote` #-v mostra progresso

`debsnap -v --first 0.0.1-1 nomedopacote |sort -V` #determina a partir de tal ponto e sort -v em ordem de versões

`debsnap -v --last 0.1.1-1 nomedopacote`

### ALTERAÇÃO DO REPOSITÓRIO REMOTO

- Verificar o origin atual e remover:

`git remote -v`

`git remote rm origin`

`git remote -v`

- Configurar novo repositório:

`git remote add origin <endereço>`

`git remote -v`

### CRIAR BRANCHS DEBIAN/MASTER UPSTREAM PRISTINE-TAR

- clonar o repositório

`git checkout -b debian/master` #para criar nova branch com conteúdo da anterior

- fazer o push para enviar para repositório e ir la no site tornar o debian/master como padrão,
e depois apagar no site a branch antiga e depois clonar novamente

- baixar o orig.tar.gz do upstream (tarball)

`git checkout --orphan upstream` #para criar a branch órfã

`git rm -rf .` #apagar todos arquivos

`git commit --allow-empty -m "Initial empty branch"` #criar primeiro commit na raiz na branch vazia

`git checkout debian/master`

`git tag -d upstream...` #remover a tag, caso a tag upstream da versão já exista

`gbp import-orig --no-merge ../pacote_0.0.1.orig.tar.gz` #para importar o orig.tar.gz

### CRIAR BRANCH PRISTINE-TAR

`gbp pristine-tar commit ../orig.tarball`


## CONFIGURAÇÃO PADRÃO BRANCH NO SALSA/DEBIAN

- debian/master:

    Allowed to merge -> Maintainers

    Allowed to push  -> Developers + Maintainers