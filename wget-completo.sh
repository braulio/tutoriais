#!/usr/bin/env bash
# ---------------------------------------------------------------
# Script   : wget-completo.sh
# Descrição: script para melhor aproveitamento do programa wget com parametros para ser completo.
# Versão   : 0.1
# Autor    : Braulio Henrique Marques Souto <braulio@disroot.org>
# Data     : 05/12/2022
# Licença  : BSD-3-clause
#
# Copyright 2022 Braulio Henrique Marques Souto <braulio@disroot.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the University nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ---------------------------------------------------------------
# Uso: ./wget-completo.sh
# ---------------------------------------------------------------

# - Variaveis ------------------------------------------------------------

dependencies=(wget)
err_dep='Dependência(s) não encontrada(s):'
msg[1]='ERRO: Instale os pacotes que contenham os programans citados acima.'
msg[2]='ERRO: É necessário colocar um endereço de site!'
msg[3]='ERRO: É necessário colocar pelo menos um dos formatos disponíveis!'

# - Vetores Associativos -------------------------------------------------

declare -A pgt
pgt[FORMATO]='Digite um dos seguintes formatos desejado podendo ser de um até os tres disponíveis
mp4,odp,pdf separando os formatos por vírgula:'
pgt[SITE]='Digite o endereço do site para baixar:'

# - Funcoes --------------------------------------------------------------

dep_check() {
    local dep not_found
    for dep in "${dependencies[@]}"; do
        command -v $dep &> /dev/null || not_found+="$dep "
    done
    [[ "$not_found" ]] && { echo "$err_dep $not_found"; die 1; }
}

die() {
    echo "${msg[$1]}"
    exit $1
}

pergunta() {
    echo "${pgt[$1]}"
    read -r $1
}

verify_formato() {
    while [[ -z $FORMATO ]]; do
	echo ${msg[3]}
	pergunta FORMATO
    done
}

verify_site() {
    while [[ -z $SITE ]]; do
	echo ${msg[2]}
	pergunta SITE
    done
}

main() {
    wget --accept $FORMATO --mirror --page- --adjust-extension --convert-links --backup-converted --no-parent $SITE
}

# - Principal ------------------------------------------------------------

dep_check

pergunta SITE
verify_site

#pergunta FORMATO
#verify_formato

main
